/*
 * The MIT License
 *
 * Copyright 2015 Maximillian M..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.isitsibfu.unittesting;

import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

/**
 *
 * @author Maximillian M.
 */
@RunWith(Parameterized.class)
public class SetParametrizedTest {
    @Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
            { 
                new Set<Integer>().append(0).append(1), 
                new Set<Integer>().append(2), 
                new Set<Integer>()
            },
            {
                new Set<Integer>().append(0).append(1),
                new Set<Integer>().append(1).append(2),
                new Set<Integer>().append(1)
            },
            {
                new Set<Integer>().append(0).append(1),
                new Set<Integer>().append(0).append(1).append(2),
                new Set<Integer>().append(0).append(1)
            },
            {
                new Set<String>().append("Foo").append("Bar"),
                new Set<String>().append("Bar").append("Baz"),
                new Set<String>().append("Bar")
            }
        });
    }
 
    private final Set set1;
    private final Set set2;
    private final Set expectation;
    
    public SetParametrizedTest(Set set1, Set set2, Set expectation) {
        this.set1 = set1;
        this.set2 = set2;
        this.expectation = expectation;
    }
    
    @Test
    public void testIntersection() {
        assertEquals(expectation, set1.intersect(set2));
    }
}
