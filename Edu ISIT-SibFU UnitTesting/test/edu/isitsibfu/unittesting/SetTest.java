/*
 * The MIT License
 *
 * Copyright 2015 Maximillian M..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.isitsibfu.unittesting;

import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maximillian M.
 */
public class SetTest {
    /**
     * Test of size method, of class Set.
     */
    @Test
    public void testSize() {
        Set set = new Set();
        assertEquals(0, set.size());
        
        set.add(0);
        set.add(1);
        set.add(2);
        
        assertEquals(3, set.size());
    }

    /**
     * Test of isEmpty method, of class Set.
     */
    @Test
    public void testIsEmpty() {
        Set set = new Set();
        assertTrue(set.isEmpty());
        
        set.add(1);
        assertFalse(set.isEmpty());
    }

    /**
     * Test of contains method, of class Set.
     */
    @Test
    public void testContains() {
        Set<String> set = new Set<>();
        set.add("Foo");
        set.add("Bar");
        set.add("Baz");
        assertFalse(set.contains("Quux"));
        
        set.add("Quux");
        assertTrue(set.contains("Quux"));
    }

    /**
     * Test of toArray method, of class Set.
     */
    @Test
    public void testToArray() {
        Set set = new Set();
        set.add(42);
        set.add(35);
        
        Object[] expResult = new Object[] {42, 35};
        Object[] result = set.toArray();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of add method, of class Set.
     */
    @Test
    public void testAdd() {
        Set<Integer> set = new Set<>();
        boolean res = set.add(42);
        assertTrue("Item was successfully added in collection", res);
        assertEquals(1, set.size());
        
        res = set.add(42);
        assertFalse("Set already contains such item", res);
    }

    /**
     * Test of fluent append method, of class Set.
     */
    @Test
    public void testAppend() {
        Set<Integer> set = new Set<>();
        assertEquals(set, set.append(42));
    }

    /**
     * Test of remove method, of class Set.
     */
    @Test
    public void testRemove() {
        Set set = new Set();
        set.add(42);
        set.add(14);
        
        set.remove(42);
        assertEquals(1, set.size());
        assertFalse(set.contains(42));
    }

    /**
     * Test of containsAll method, of class Set.
     */
    @Test
    public void testContainsAll() {
        Set a = new Set();
        a.append(42).append(88);
        
        Set b = new Set();
        b.append(42).append(88).append(32);
        
        assertTrue("{42, 88} is subset of {42, 88, 32}", b.containsAll(a));
        assertFalse("{42, 88, 32} isn't subset of {42, 88}", a.containsAll(b));
        assertTrue("Reflexivity", b.containsAll(b));
    }

    /**
     * Test of addAll method, of class Set.
     */
    @Test
    public void testAddAll() {
        Set a = new Set();
        a.addAll(Arrays.asList(new Integer[] { 42, 14 }));
        assertEquals("Add list", 2, a.size());
        
        a = new Set();
        Set b = new Set();
        b.append(3).append(0).append(333);
        a.addAll(b);
        assertEquals("Add other set", 3, a.size());
        
        a = new Set();
        a.append(3).append(333);
        a.addAll(b);
        assertEquals("Set must contain only unique items", 3, a.size());
    }

    /**
     * Test of removeAll method, of class Set.
     */
    @Test
    public void testRemoveAll() {
        Set set = new Set();
        set.append(42).append(0).append(7498465);
        set.removeAll(Arrays.asList(new Integer[] { 42, 0 }));
        assertEquals(1, set.size());
    }

    /**
     * Test of clear method, of class Set.
     */
    @Test
    public void testClear() {
        Set set = new Set();
        set.append(42).append(0).append(7498465);
        set.clear();
        assertEquals(0, set.size());
    }

    /**
     * Test of union method, of class Set.
     */
    @Test
    public void testUnion() {
        Set a = new Set();
        a.append(0).append(1);
        Set b = new Set();
        b.append(2).append(3);
        
        Set exp = new Set();
        exp.append(0).append(1).append(2).append(3);
        Set res = a.union(b);
        
        assertEquals(exp, res);
    }

    /**
     * Test of intersect method, of class Set.
     */
    @Test
    public void testIntersect() {
        Set a = new Set();
        a.append(0).append(1).append(2);
        Set b = new Set();
        b.append(2).append(3);
        
        Set exp = new Set();
        exp.append(2);
        Set res = a.intersect(b);
        
        assertEquals(exp, res);
    }

    /**
     * Test of subtract method, of class Set.
     */
    @Test
    public void testSubtract() {
        Set a = new Set();
        a.append(0).append(1).append(2);
        Set b = new Set();
        b.append(2).append(3);
        
        Set exp = new Set();
        exp.append(0).append(1);
        Set res = a.subtract(b);
        
        assertEquals(exp, res);
    }

    /**
     * Test of equals method, of class Set.
     */
    @Test
    public void testEquals() {        
        Set a = new Set();
        a.append(0).append(1).append(2);
        Set b = new Set();
        b.append(0).append(1);
        assertFalse(a == b);
        b.add(2);
        assertEquals(b, a);
    }
}
