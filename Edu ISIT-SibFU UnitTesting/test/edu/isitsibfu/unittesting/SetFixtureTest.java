/*
 * The MIT License
 *
 * Copyright 2015 Maximillian M..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.isitsibfu.unittesting;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maximillian M.
 */
public class SetFixtureTest {
    private Set<Integer> set1;
    private Set<Integer> set2;
    
    @Before
    public void setUp() {   
        set1 = new Set<Integer>().append(0).append(1).append(2);
        set2 = new Set<Integer>().append(2).append(3).append(4);
    }
    
    @Test
    public void testUnion() {
        Set<Integer> union = new Set<Integer>().append(0).append(1).append(2)
                .append(3).append(4);
        Set<Integer> result = set1.union(set2);
        assertEquals(union, result);
    }
    
    @Test
    public void testIntersect() {
        Set<Integer> intersect = new Set<Integer>().append(2);
        Set<Integer> result = set1.intersect(set2);
        assertEquals(intersect, result);
    }
    
    @Test
    public void testSubtract() {
        Set<Integer> subtract = new Set<Integer>().append(0).append(1);
        Set<Integer> result = set1.subtract(set2);
        assertEquals(subtract, result);
    }
}
