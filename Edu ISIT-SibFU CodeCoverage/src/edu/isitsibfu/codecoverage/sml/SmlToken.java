/*
 * The MIT License
 *
 * Copyright 2015 Maximillian M..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.isitsibfu.codecoverage.sml;

/**
 *
 * @author Maximillian M.
 */
public class SmlToken {
    String lexem;
    int line;
    int column;
    
    SmlTokenType type;
    
    public SmlToken(SmlTokenType type) {
        this(type, 0, 0);
    }
    public SmlToken(SmlTokenType type, String lexem) {
        this.type = type;
        this.lexem = lexem;
    }
    public SmlToken(SmlTokenType type, int line, int column) {
        this.type = type;
        this.line = line;
        this.column = column;
        switch (type) {
            case BraceCL:
                lexem = "{";
                break;
            case BraceCR:
                lexem = "}";
                break;
            case Equality:
                lexem = "=";
                break;
            case Comma:
                lexem = ",";
                break;
            case Intersect:
                lexem = "#";
                break;
            case Union:
                lexem = "+";
                break;
            case IsSubset:
                lexem = "?";
                break;
            case Subtract:
                lexem = "-";
                break;
            default:
                lexem = "";
        }
    }
    public SmlToken(SmlTokenType type, String lexem, int line, int column) {
        this.type = type;
        this.lexem = lexem;
        this.line = line;
        this.column = column;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof SmlToken)) 
            return false;
        
        SmlToken token = (SmlToken) obj;
        boolean equal = token.type == this.type;
        equal &= token.lexem.equals(this.lexem);
        
        return equal;
    }

    @Override
    public int hashCode() {
        return type.hashCode() & lexem.hashCode();
    }
}


