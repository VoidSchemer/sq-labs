/*
 * The MIT License
 *
 * Copyright 2015 Maximillian M..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.isitsibfu.codecoverage.sml;

/**
 *
 * @author Maximillian M.
 */
public class SmlLexer {
    String source;
    int index;
    int line;
    int column;
    
    public SmlLexer(String source) {
        this.source = source.replace(" ", "");
        index = 0;
        line = 0;
        column = 0;
    }
    
    public SmlToken getToken() {
        if (index >= source.length()) {
            return new SmlToken(SmlTokenType.EOF, line, column);
        }
        
        SmlToken token = new SmlToken(SmlTokenType.Error, line, column);
        switch (source.charAt(index)) {
            case '\n':
                index++; column = 0; line++;
                return getToken();
            case '{':
                token = new SmlToken(SmlTokenType.BraceCL, line, column);
                break;
            case '}':
                token = new SmlToken(SmlTokenType.BraceCR, line, column);
                break;
            case ',':
                token = new SmlToken(SmlTokenType.Comma, line, column);
                break;
            case '=':
                token = new SmlToken(SmlTokenType.Equality, line, column);
                break;
            case '+':
                token = new SmlToken(SmlTokenType.Union, line, column);
                break;
            case '-':
                token = new SmlToken(SmlTokenType.Subtract, line, column);
                break;
            // kek 1
            case '#':
                token = new SmlToken(SmlTokenType.Intersect, line, column);
                break;
            // kek 2
            case '?':
                token = new SmlToken(SmlTokenType.IsSubset, line, column);
                break;
            default:
                if (Character.isDigit(source.charAt(index))) {
                    token = getInteger();
                }
        }
        
        column += token.lexem.length();
        index += token.lexem.length();
        return token;
    }
    
    private SmlToken getInteger() {
        StringBuilder builder = new StringBuilder();
        int tmp = index;
        while (true) {
            if (tmp >= source.length()) {
                break;
            }
            
            char sym = source.charAt(tmp);
            if (Character.isDigit(sym)) {
                builder.append(sym);
                tmp++;
            } else {
                break;
            }
        }
        
        return new SmlToken(SmlTokenType.Integer, builder.toString(), line, column);
    }
}
