/*
 * The MIT License
 *
 * Copyright 2015 Maximillian M..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.isitsibfu.codecoverage.sml;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maximillian M.
 */
public class SmlTokenTest {
    /**
     * Test of equals method, of class SmlToken.
     */
    @Test
    public void testEquals() {
        // Token(x) == Token(x)
        SmlToken tokenA = new SmlToken(SmlTokenType.Intersect);
        assertEquals(tokenA, tokenA);
        
        // Token(x1) == Token(x2)
        SmlToken tokenB = new SmlToken(SmlTokenType.Intersect);
        assertEquals(tokenA, tokenB);
        
        // Token(x1) of type1 != Token(x1) of type2
        tokenB = new SmlToken(SmlTokenType.Integer, "42");
        assertNotEquals(tokenA, tokenB);
        
        // Token(x) != non Token
        assertNotEquals(tokenA, 42);
    }

    /**
     * Test of hashCode method, of class SmlToken.
     */
    @Test
    public void testHashCode() {
        SmlToken tokenA = new SmlToken(SmlTokenType.Integer, "42");
        SmlToken tokenB = new SmlToken(SmlTokenType.Integer, "42");
        assertEquals(tokenA.hashCode(), tokenB.hashCode());
        
        tokenB = new SmlToken(SmlTokenType.Integer, "1");
        assertNotEquals(tokenA.hashCode(), tokenB.hashCode());
    }
}
