/*
 * The MIT License
 *
 * Copyright 2015 Maximillian M..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.isitsibfu.codecoverage.sml;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maximillian M.
 */
public class SmlLexerTest {
    public ArrayList<SmlToken> lex(String src) {
        SmlLexer lexer = new SmlLexer(src);
        ArrayList<SmlToken> tokens = new ArrayList<>();
        OUTER:
        while (true) {
            SmlToken token = lexer.getToken();
            switch (token.type) {
                case Error:
                    System.out.println("Error at " + token.line + ":" + token.column);
                    tokens.add(token);
                    break OUTER;
                case EOF:
                    break OUTER;
                default:
                    System.out.println(token.lexem + " at " + token.line + ":" + token.column);
                    tokens.add(token);
                    break;
            }
        }
        
        return tokens;
    }
    
    @Test
    public void lexEmptyString() {
        ArrayList<SmlToken> tokens = lex("");
        assertTrue(tokens.isEmpty());
    }
    
    @Test
    public void lexEmptySet() {
        ArrayList<SmlToken> tokens = lex("{}");
        assertEquals(SmlTokenType.BraceCL, tokens.get(0).type);
        assertEquals(SmlTokenType.BraceCR, tokens.get(1).type);
    }
    
    @Test
    public void lexInteger() {
        ArrayList<SmlToken> tokens = lex("1");
        assertEquals(new SmlToken(SmlTokenType.Integer, "1"), tokens.get(0));
        tokens = lex("42");
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(0));
    }
    
    @Test
    public void lexSetWithOneItem() {
        ArrayList<SmlToken> tokens = lex("{42}");
        assertEquals(SmlTokenType.BraceCL, tokens.get(0).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(1));
        assertEquals(SmlTokenType.BraceCR, tokens.get(2).type);
    }
    
    @Test
    public void lexSetWithSeveralItems() {
        ArrayList<SmlToken> tokens = lex("{322,42}");
        assertEquals(SmlTokenType.BraceCL, tokens.get(0).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "322"), tokens.get(1));
        assertEquals(SmlTokenType.Comma, tokens.get(2).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(3));
        assertEquals(SmlTokenType.BraceCR, tokens.get(4).type);
    }
    
    @Test
    public void lexUnionOperation() {
        ArrayList<SmlToken> tokens = lex("{322,42}+{228}");
        assertEquals(SmlTokenType.BraceCL, tokens.get(0).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "322"), tokens.get(1));
        assertEquals(SmlTokenType.Comma, tokens.get(2).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(3));
        assertEquals(SmlTokenType.BraceCR, tokens.get(4).type);
        
        assertEquals(SmlTokenType.Union, tokens.get(5).type);  
        
        assertEquals(SmlTokenType.BraceCL, tokens.get(6).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "228"), tokens.get(7));
        assertEquals(SmlTokenType.BraceCR, tokens.get(8).type);
    }
    
    @Test
    public void lexSubtractOperation() {
        ArrayList<SmlToken> tokens = lex("{322,42}-{228}");
        assertEquals(SmlTokenType.BraceCL, tokens.get(0).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "322"), tokens.get(1));
        assertEquals(SmlTokenType.Comma, tokens.get(2).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(3));
        assertEquals(SmlTokenType.BraceCR, tokens.get(4).type);
        
        assertEquals(SmlTokenType.Subtract, tokens.get(5).type);  
        
        assertEquals(SmlTokenType.BraceCL, tokens.get(6).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "228"), tokens.get(7));
        assertEquals(SmlTokenType.BraceCR, tokens.get(8).type);
    }
    
    @Test
    public void lexIntersectOperation() {
        ArrayList<SmlToken> tokens = lex("{322,42}#{228}");
        assertEquals(SmlTokenType.BraceCL, tokens.get(0).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "322"), tokens.get(1));
        assertEquals(SmlTokenType.Comma, tokens.get(2).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(3));
        assertEquals(SmlTokenType.BraceCR, tokens.get(4).type);
        
        assertEquals(SmlTokenType.Intersect, tokens.get(5).type);  
        
        assertEquals(SmlTokenType.BraceCL, tokens.get(6).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "228"), tokens.get(7));
        assertEquals(SmlTokenType.BraceCR, tokens.get(8).type);
    }
    
    @Test
    public void lexIsSubsetOperation() {
        ArrayList<SmlToken> tokens = lex("{322,42}?{228}");
        assertEquals(SmlTokenType.BraceCL, tokens.get(0).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "322"), tokens.get(1));
        assertEquals(SmlTokenType.Comma, tokens.get(2).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(3));
        assertEquals(SmlTokenType.BraceCR, tokens.get(4).type);
        
        assertEquals(SmlTokenType.IsSubset, tokens.get(5).type); 
        
        assertEquals(SmlTokenType.BraceCL, tokens.get(6).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "228"), tokens.get(7));
        assertEquals(SmlTokenType.BraceCR, tokens.get(8).type);
    }
    
    @Test
    public void lexEqualityOperation() {
        ArrayList<SmlToken> tokens = lex("{42}={42}");
        assertEquals(SmlTokenType.BraceCL, tokens.get(0).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(1));
        assertEquals(SmlTokenType.BraceCR, tokens.get(2).type);
                
        assertEquals(SmlTokenType.Equality, tokens.get(3).type); 

        assertEquals(SmlTokenType.BraceCL, tokens.get(4).type);
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(5));
        assertEquals(SmlTokenType.BraceCR, tokens.get(6).type);
    }
    
    @Test
    public void lexMultiline() {
        ArrayList<SmlToken> tokens = lex("42" + '\n' + "322");
        assertEquals(new SmlToken(SmlTokenType.Integer, "42"), tokens.get(0));
        assertEquals(new SmlToken(SmlTokenType.Integer, "322"), tokens.get(1));
        assertEquals(1, tokens.get(1).line);
    }
    
    @Test
    public void lexError() {
        ArrayList<SmlToken> tokens = lex("foo");
        assertEquals(new SmlToken(SmlTokenType.Error), tokens.get(0));
    }
}
