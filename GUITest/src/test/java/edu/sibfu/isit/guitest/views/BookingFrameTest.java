/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.guitest.views;

import edu.sibfu.isit.guitest.controllers.BookingControllerStub;
import edu.sibfu.isit.guitest.models.BookingOrder;
import java.util.Arrays;
import org.fest.swing.annotation.RunsInEDT;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.fixture.JTextComponentFixture;
import org.fest.swing.junit.testcase.FestSwingJUnitTestCase;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.swing.edt.GuiActionRunner.execute;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.JComboBoxFixture;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Maximillian M.
 */
public class BookingFrameTest extends FestSwingJUnitTestCase {
    private static final String TB_DATEBEGIN;
    private static final String TB_LENGTH;
    private static final String CB_COUNTRY;
    private static final String CB_PEOPLE;
    private static final String CB_HOTELRATE;
    private static final String CB_ROOMRATE;
    
    private static BookingControllerStub ctrl;
    private FrameFixture frame;
    
    static {
        ctrl = new BookingControllerStub();
        TB_DATEBEGIN = "dateBegin";
        TB_LENGTH = "length";
        CB_COUNTRY = "country";
        CB_HOTELRATE = "hotelRate";
        CB_ROOMRATE = "roomRate";
        CB_PEOPLE = "countOfPeople";
    }
    
    @Override
    protected void onSetUp() {
        frame = new FrameFixture(robot(), runBookingFrame());
        frame.show();
    }
   
    @RunsInEDT
    private static BookingFrame runBookingFrame() {
        return execute(new GuiQuery<BookingFrame>() {
            @Override
            protected BookingFrame executeInEDT() throws Throwable {
                return new BookingFrame(ctrl);
            }
        });
    }
    
    public void visitTextBox(String name, String text) {
        JTextComponentFixture tb = frame.textBox(name);
        tb.setText(text);
        assertThat(tb.text().contains(text));
    }
    public void visitComboBox(String name, String text) {
        JComboBoxFixture cb = frame.comboBox(name);
        cb.selectItem(text);
        int id = Arrays.asList(cb.contents()).indexOf(text);
        assertThat(cb.valueAt(id)).isEqualTo(text);
    }
    
    @Test
    public void foo() {
        visitTextBox("length", "4");
    }
    
    @Test
    public void testFrame() {
        visitTextBox(TB_DATEBEGIN, "12.12.12");
        visitTextBox(TB_LENGTH, "7");
        visitComboBox(CB_COUNTRY, "Чехия");
        visitComboBox(CB_PEOPLE, "1");
        visitComboBox(CB_HOTELRATE, "5*");
        visitComboBox(CB_ROOMRATE, "Superior");
    }
    
    @Test 
    public void testResult() {
        BookingOrder excepted = new BookingOrder("12.12.12", "7", "Чехия", "5*", "Superior", "1", null);
        ctrl.reset();
        {
            visitTextBox(TB_DATEBEGIN, "12.12.12");
            visitTextBox(TB_LENGTH, "7");
            visitComboBox(CB_COUNTRY, "Чехия");
            visitComboBox(CB_PEOPLE, "1");
            visitComboBox(CB_HOTELRATE, "5*");
            visitComboBox(CB_ROOMRATE, "Superior");
            frame.button("search").click();
        }
        ctrl.await();
        
        BookingOrder actual = ctrl.getOrder();
        Assert.assertEquals(excepted, actual);
    }
}
