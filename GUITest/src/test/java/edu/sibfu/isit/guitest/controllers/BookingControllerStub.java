/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.guitest.controllers;

import edu.sibfu.isit.guitest.events.BookingEventEmitter;
import edu.sibfu.isit.guitest.models.BookingOrder;
import java.util.Observable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.JOptionPane;

/**
 *
 * @author Maximillian M.
 */
public class BookingControllerStub implements IController {
    private BookingOrder order;
    
    private final int COUNT;
    private CountDownLatch latch;
    private Lock lock;  
    
    {
        COUNT = 1;
        latch = new CountDownLatch(COUNT);
        lock = new ReentrantLock();
    }
    
    public void reset() {
        latch = new CountDownLatch(COUNT);
        order = null;
    }
    
    public void await() {
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
    }
    
    public void free() {
        latch.countDown();
    }
    
    @Override
    public void update(Observable o, Object arg) {
        if (!(o instanceof BookingEventEmitter)) {
            JOptionPane.showMessageDialog(null, "Unknown event emitter");
            return;
        }
        
        if (!(arg instanceof BookingOrder)) {
            JOptionPane.showMessageDialog(null, "Unknown event argument");
            return;
        }
        
        order = (BookingOrder) arg;
        free();
    }
    
    public BookingOrder getOrder() {
        return order;
    }
    
}
