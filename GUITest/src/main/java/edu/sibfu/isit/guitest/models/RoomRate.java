/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.guitest.models;

import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Maximillian M.
 */
public class RoomRate {
    private String value;
    
    public RoomRate(String rate) {
        if (!verify(rate)) {
            throw new IllegalArgumentException();
        }
        
        value = rate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RoomRate other = (RoomRate) obj;
        return Objects.equals(this.value, other.value);
    }
    
    public String getRate() {
        return value;
    }
    
    public static boolean verify(String roomRate) {
        return Arrays
                .stream(getRoomRates(false))
                .filter((rr) -> rr.equals(roomRate))
                .findFirst().isPresent();
    }
    
    private static String[] cache_rate;
    public static String[] getRoomRates(boolean invalidateCache) {
        if (cache_rate != null && !invalidateCache) {
            return cache_rate;
        }
        
        String[] rates = new String[8];
        rates[0] = "Standard";
        rates[1] = "Superior";
        rates[2] = "De Luxe";
        rates[3] = "Executive Room";
        rates[4] = "Family Room";
        rates[5] = "Business Room";
        rates[6] = "Duplex";
        rates[7] = "President Suites";
        
        cache_rate = rates;
        return rates;
    }
}
