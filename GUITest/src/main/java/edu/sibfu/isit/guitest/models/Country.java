/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.guitest.models;

import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Maximillian M.
 */
public class Country {
    private String value;
    
    public Country(String country) {
        if (!verify(country)) {
            throw new IllegalArgumentException();
        }
        
        value = country;
    }
    
    public String get() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Country other = (Country) obj;
        return Objects.equals(this.value, other.value);
    }
    
    public static boolean verify(String country) {
        return Arrays
            .stream(getCountries(false))
            .filter((str) -> str.equals(country))
            .findFirst().isPresent();
    }
    
    private static String[] cache_countries;
    public static String[] getCountries(boolean invalidateCache) {
        if (cache_countries != null && !invalidateCache) {
            return cache_countries;
        }
        
        String[] countries = new String[4];
        countries[0] = "Болгария";
        countries[1] = "Ирландия";
        countries[2] = "Черногория";
        countries[3] = "Чехия";
        
        cache_countries = countries;
        return countries;
    }
}
