/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.guitest.models;

import edu.sibfu.isit.guitest.views.BookingFrame;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maximillian M.
 */
public class BookingOrder {
    private static final DateFormat DATE_FORMAT;
        
    private final Date date;
    private final int length;
    private final Country country;
    private final HotelRate hotelRate;
    private final RoomRate roomRate;
    private final int people;
           
    private final ArrayList<String> errors;

    static {
        DATE_FORMAT = new SimpleDateFormat("MM.dd.yy");
    }
    
    private Date dateParse(String date, ArrayList<String> err) {
        if (date.trim().length() == 0) {
            err.add("Введите дату начала отпуска");
            return null;
        }
        
        try {
            return DATE_FORMAT.parse(date);
        } catch (ParseException ex) {
            err.add("Неправильный формат даты. Должен быть: ММ.ДД.ГГ");
            Logger.getLogger(BookingOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    private int lengthParse(String length, ArrayList<String> err) {
        if (length.trim().length() == 0) {
            err.add("Введите длительность отпуска");
            return 0;
        }
        
        int l = 0;
        try {
            l = Integer.parseInt(length);
        } catch (NumberFormatException ex) {
            err.add("Количество дней: Вы ввели не число =(");
            Logger.getLogger(BookingOrder.class.getName()).log(Level.SEVERE, null, ex);
            return l;
        }
        
        if (l < 1) {
            err.add("Количество дней не может быть меньше нуля");
        }
        
        return l;
    }
    private int peopleParse(String people, ArrayList<String> err) {
        if (people.trim().length() == 0) {
            err.add("Введите количество людей");
            return 0;
        }
        
        int p = 0;
        try {
            p = Integer.parseInt(people);
        } catch (NumberFormatException ex) {
            err.add("Количество людей: Вы ввели не число =(");
            Logger.getLogger(BookingOrder.class.getName()).log(Level.SEVERE, null, ex);
            return p;
        }
        
        if (p < 1) {
            err.add("Количество людей не может быть меньше нуля");
        } else if (p > 4) {
            err.add("Извините, но не больше 4 человек");
        }
        return p;
    }
    private Country countryParse(String country, ArrayList<String> err) {
        try {
            return new Country(country);
        } catch (IllegalArgumentException ex) {
            err.add("Мы не предоставляем путёвки в выбранную вами страну");
            Logger.getLogger(BookingOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private HotelRate hotelRateParse(String hotelRate, ArrayList<String> err) {
        try {
            return new HotelRate(hotelRate);
        } catch (IllegalArgumentException ex) {
            err.add("Мы не знаем такого класса отеля =(");
            Logger.getLogger(BookingOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private RoomRate roomRateParse(String roomRate, ArrayList<String> err) {
        try {
            return new RoomRate(roomRate);
        } catch (IllegalArgumentException ex) {
            err.add("Мы не знаем такого класса номеров =(");
            Logger.getLogger(BookingOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String toString() {
        return "BookingOrder{" + "date=" + date + ", length=" + length + ", country=" + country + ", hotelRate=" + hotelRate + ", roomRate=" + roomRate + ", people=" + people + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.date);
        hash = 83 * hash + this.length;
        hash = 83 * hash + Objects.hashCode(this.country);
        hash = 83 * hash + Objects.hashCode(this.hotelRate);
        hash = 83 * hash + Objects.hashCode(this.roomRate);
        hash = 83 * hash + this.people;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookingOrder other = (BookingOrder) obj;
        if (this.length != other.length) {
            return false;
        }
        if (this.people != other.people) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.hotelRate, other.hotelRate)) {
            return false;
        }
        return Objects.equals(this.roomRate, other.roomRate);
    }
    
    public BookingOrder(String date, 
        String length, 
        String country,
        String hotelRate,
        String roomRate,
        String people,
        Consumer<ArrayList<String>> errorCallback
    ) {
        errors = new ArrayList<>();
        this.date = dateParse(date, errors);
        this.length = lengthParse(length, errors);
        this.people = peopleParse(people, errors);
        this.country = countryParse(country, errors);
        this.hotelRate = hotelRateParse(hotelRate, errors);
        this.roomRate = roomRateParse(roomRate, errors);
        
        if (!errors.isEmpty()) {
            if (errorCallback != null) {
                errorCallback.accept(errors);
            }
            throw new IllegalArgumentException();
        }
    }
    
    public ArrayList<String> getErrorList() {
        return errors;
    }
}
