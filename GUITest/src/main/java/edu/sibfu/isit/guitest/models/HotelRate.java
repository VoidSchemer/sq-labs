/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.guitest.models;

import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Maximillian M.
 */
public class HotelRate {
    private String value;
    
    public HotelRate(String rate) {
        if (!verify(rate)) {
            throw new IllegalArgumentException();
        }
        
        value = rate;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HotelRate other = (HotelRate) obj;
        return Objects.equals(this.value, other.value);
    }
    
    public String getRate() {
        return value;
    }
    
    public static boolean verify(String hotelRate) {
        return Arrays
                .stream(getHotelRates(false))
                .filter((hr) -> hr.equals(hotelRate))
                .findFirst().isPresent();
    }
    
    private static String[] cache_rate;
    public static String[] getHotelRates(boolean invalidateCache) {
        if (cache_rate != null && !invalidateCache) {
            return cache_rate;
        }
        
        String[] rates = new String[5];
        rates[4] = "1*";
        rates[3] = "2*";
        rates[2] = "3*";
        rates[1] = "4*";
        rates[0] = "5*";
        
        cache_rate = rates;
        return rates;
    }
}
