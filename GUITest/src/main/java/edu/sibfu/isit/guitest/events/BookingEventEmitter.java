/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.guitest.events;

import edu.sibfu.isit.guitest.controllers.IController;
import edu.sibfu.isit.guitest.models.BookingOrder;
import java.util.Observable;

/**
 *
 * @author Maximillian M.
 */
public class BookingEventEmitter extends Observable {
    public BookingEventEmitter(IController controller) {
        addObserver(controller);
    }
    
    public void search(BookingOrder data) {
        setChanged();
        notifyObservers(data);
    }
}
