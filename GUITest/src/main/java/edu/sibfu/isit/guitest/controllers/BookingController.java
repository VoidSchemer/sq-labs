/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.guitest.controllers;

import edu.sibfu.isit.guitest.events.BookingEventEmitter;
import edu.sibfu.isit.guitest.models.BookingOrder;
import java.util.Observable;
import javax.swing.JOptionPane;

/**
 *
 * @author Maximillian M.
 */
public class BookingController implements IController {

    @Override
    public void update(Observable o, Object arg) {
        if (!(o instanceof BookingEventEmitter)) {
            JOptionPane.showMessageDialog(null, "Unknown event emitter");
            return;
        }
        
        if (!(arg instanceof BookingOrder)) {
            JOptionPane.showMessageDialog(null, "Unknown event argument");
            return;
        }
        
        if (arg == null) {
            JOptionPane.showMessageDialog(null, "NullReference", "Eror", JOptionPane.ERROR_MESSAGE);
        } else {
            BookingOrder order = (BookingOrder) arg;
            JOptionPane.showMessageDialog(null, order.toString(), "Info", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
