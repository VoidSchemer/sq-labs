/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.components;

import edu.emory.mathcs.backport.java.util.Arrays;
import edu.sibfu.isit.mocks.lang.Set;
import edu.sibfu.isit.mocks.smlc.IParser;
import edu.sibfu.isit.mocks.smlc.tokens.*;
import java.util.Collection;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

/**
 *
 * @author Maximillian M.
 */
@RunWith(Parameterized.class)
public class CalcTest {
    private IParser parser;
    private Calc calc;
    
    private static final String initInput = "{0, 42}";
    private static final SetToken initToken = new SetToken(new Set().append(0).append(42));
    private static final String argInput = "{42, 35}";
    private static final SetToken argToken = new SetToken(new Set().append(42).append(35));
    
    private final String opInput;
    private final Token opToken;
    private final Calc.Status expStatus;
    private final Set expSet;
    @Parameters
    public static Collection<Object[]> data() {
        Set initSet = CalcTest.initToken.value();
        Set argSet = CalcTest.argToken.value();
        return Arrays.asList(new Object[][] {
            {"+", new OpToken(OpCode.Union), Calc.Status.Success, initSet.union(argSet)},
            {"-", new OpToken(OpCode.Subtract), Calc.Status.Success, initSet.subtract(argSet)},
            {"#", new OpToken(OpCode.Intersect), Calc.Status.Success, initSet.intersect(argSet)},
            {"?", new OpToken(OpCode.Invalid), Calc.Status.Err_BadOperation, initSet},
        });
    }
    
    public CalcTest(String input, Token token, Calc.Status expStatus, Set expSet) {
        this.opInput = input;
        this.opToken = token;
        this.expStatus = expStatus;
        this.expSet = expSet;
    }
    
    @Before
    public void setUp() {
        parser = EasyMock.createMock(IParser.class);
        calc = new Calc(parser);
        EasyMock.expect(parser.parse(initInput)).andReturn(initToken);
        EasyMock.expect(parser.parse(opInput)).andReturn(opToken);
        EasyMock.expect(parser.parse(argInput)).andReturn(argToken);
        EasyMock.replay(parser);
    }
    
    @Test
    public void test() {
        calc.step(initInput);
        calc.step(opInput);
        Calc.Status status = calc.step(argInput);
        
        assertEquals(status, expStatus);
        assertEquals(calc.getAcc(), expSet);
    }
}
