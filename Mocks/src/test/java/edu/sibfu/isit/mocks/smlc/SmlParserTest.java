/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.smlc;

import edu.emory.mathcs.backport.java.util.Arrays;
import edu.sibfu.isit.mocks.lang.Set;
import edu.sibfu.isit.mocks.smlc.tokens.*;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

/**
 *
 * @author Maximillian M.
 */
@RunWith(Parameterized.class)
public class SmlParserTest {
    private SmlParser parser;
    private final String input;
    private final Token expected;
    
    @Before
    public void setUp() {
        parser = new SmlParser();
    }
    
    @Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][] {
            {"{42}", new SetToken(new Set().append(42))},
            {"+", new OpToken(OpCode.Union)},
            {"-", new OpToken(OpCode.Subtract)},
            {"#", new OpToken(OpCode.Intersect)},
            {"foo", new ErrToken()}
        });
    }
    
    public SmlParserTest(String input, Token expected) {
        this.input = input;
        this.expected = expected;
    }
    
    @Test
    public void test() {
        Token actual = parser.parse(input);
        assertEquals(expected, actual);
    }
}
