/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.components;

import edu.emory.mathcs.backport.java.util.Arrays;
import edu.sibfu.isit.mocks.lang.Set;
import edu.sibfu.isit.mocks.smlc.IParser;
import edu.sibfu.isit.mocks.smlc.tokens.*;
import java.util.Collection;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

/**
 *
 * @author Maximillian M.
 */
@RunWith(Parameterized.class)
public class CalcInitialStateTest {
    private IParser parser;
    private Calc calc;
    
    private final String input;
    private final Token token;
    private final Calc.Status expStatus;
    private final Set expSet;
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"{42,35}", new SetToken(new Set().append(42).append(35)), Calc.Status.Success, new Set().append(42).append(35)},
            {"+", new OpToken(OpCode.Union), Calc.Status.Err_SetExpected, new Set()},
            {"", new EpsToken(), Calc.Status.Err_SetExpected, new Set()},
            {"foo", new ErrToken(), Calc.Status.Err_InvalidInput, new Set()}
        });
    }
    
    public CalcInitialStateTest(String input, Token token, Calc.Status expStatus, Set expSet) {
        this.input = input;
        this.token = token;
        this.expStatus = expStatus;
        this.expSet = expSet;
    }
    
    @Before
    public void setUp() {
        parser = EasyMock.createMock(IParser.class);
        calc = new Calc(parser);
        EasyMock.expect(parser.parse(input)).andReturn(token);
        EasyMock.replay(parser);
    }
    
    @Test
    public void test() {
        Calc.Status status = calc.step(input);
        assertEquals(status, expStatus);
        assertEquals(calc.getAcc(), expSet);
    }
}
