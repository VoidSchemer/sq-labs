/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.lang;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maximillian M.
 */
public class SetTest {
    @Test
    public void testCopyConstructor() {
        Set exp = new Set();
        exp.append(42).append(35);
        ArrayList<Integer> list = new ArrayList<>();
        list.add(42);
        list.add(35);
        Set res = new Set(list);
        assertEquals(exp, res);
    }
    
    /**
     * Test of size method, of class Set.
     */
    @Test
    public void testSize() {
        Set set = new Set();
        assertEquals(0, set.size());
        
        set.add(0);
        set.add(1);
        set.add(2);
        
        assertEquals(3, set.size());
    }

    /**
     * Test of isEmpty method, of class Set.
     */
    @Test
    public void testIsEmpty() {
        Set set = new Set();
        assertTrue(set.isEmpty());
        
        set.add(1);
        assertFalse(set.isEmpty());
    }

    /**
     * Test of contains method, of class Set.
     */
    @Test
    public void testContains() {
        Set<String> set = new Set<>();
        set.add("Foo");
        set.add("Bar");
        set.add("Baz");
        assertFalse(set.contains("Quux"));
        
        set.add("Quux");
        assertTrue(set.contains("Quux"));
    }

    /**
     * Test of toArray method, of class Set.
     */
    @Test
    public void testToArray() {
        Set set = new Set();
        set.add(42);
        set.add(35);
        
        Object[] expResult = new Object[] {42, 35};
        Object[] result = set.toArray();
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testToArrayGeneric() {
        Set<Integer> set = new Set<>();
        set.add(42);
        set.add(35);
        
        Integer[] expResult = new Integer[] {42, 35};
        Integer[] result = set.toArray(expResult);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of add method, of class Set.
     */
    @Test
    public void testAdd() {
        Set<Integer> set = new Set<>();
        boolean res = set.add(42);
        assertTrue("Item was successfully added in collection", res);
        assertEquals(1, set.size());
        
        res = set.add(42);
        assertFalse("Set already contains such item", res);
    }

    /**
     * Test of fluent append method, of class Set.
     */
    @Test
    public void testAppend() {
        Set<Integer> set = new Set<>();
        assertEquals(set, set.append(42));
    }

    /**
     * Test of remove method, of class Set.
     */
    @Test
    public void testRemove() {
        Set set = new Set();
        set.add(42);
        set.add(14);
        
        set.remove(42);
        assertEquals(1, set.size());
        assertFalse(set.contains(42));
    }

    /**
     * Test of containsAll method, of class Set.
     */
    @Test
    public void testContainsAll() {
        Set a = new Set();
        a.append(42).append(88);
        
        Set b = new Set();
        b.append(42).append(88).append(32);
        
        assertTrue("{42, 88} is subset of {42, 88, 32}", b.containsAll(a));
        assertFalse("{42, 88, 32} isn't subset of {42, 88}", a.containsAll(b));
        assertTrue("Reflexivity", b.containsAll(b));
        assertTrue("Reflexivity", b.isSubset(b));
    }

    /**
     * Test of addAll method, of class Set.
     */
    @Test
    public void testAddAll() {
        Set a = new Set();
        a.addAll(Arrays.asList(new Integer[] { 42, 14 }));
        assertEquals("Add list", 2, a.size());
        
        a = new Set();
        Set b = new Set();
        b.append(3).append(0).append(333);
        a.addAll(b);
        assertEquals("Add other set", 3, a.size());
        
        a = new Set();
        a.append(3).append(333);
        a.addAll(b);
        assertEquals("Set must contain only unique items", 3, a.size());
    }

    /**
     * Test of removeAll method, of class Set.
     */
    @Test
    public void testRemoveAll() {
        Set set = new Set();
        set.append(42).append(0).append(7498465);
        set.removeAll(Arrays.asList(new Integer[] { 42, 0 }));
        assertEquals(1, set.size());
    }

    /**
     * Test of clear method, of class Set.
     */
    @Test
    public void testClear() {
        Set set = new Set();
        set.append(42).append(0).append(7498465);
        set.clear();
        assertEquals(0, set.size());
    }

    /**
     * Test of union method, of class Set.
     */
    @Test
    public void testUnion() {
        Set a = new Set();
        a.append(0).append(1);
        Set b = new Set();
        b.append(2).append(3);
        
        Set exp = new Set();
        exp.append(0).append(1).append(2).append(3);
        Set res = a.union(b);
        
        assertEquals(exp, res);
    }

    /**
     * Test of intersect method, of class Set.
     */
    @Test
    public void testIntersect() {
        Set a = new Set();
        a.append(0).append(1).append(2);
        Set b = new Set();
        b.append(2).append(3);
        
        Set exp = new Set();
        exp.append(2);
        Set res = a.intersect(b);
        
        assertEquals(exp, res);
    }

    /**
     * Test of subtract method, of class Set.
     */
    @Test
    public void testSubtract() {
        Set a = new Set();
        a.append(0).append(1).append(2);
        Set b = new Set();
        b.append(2).append(3);
        
        Set exp = new Set();
        exp.append(0).append(1);
        Set res = a.subtract(b);
        
        assertEquals(exp, res);
    }

    /**
     * Test of equals method, of class Set.
     */
    @Test
    public void testEquals() {        
        Set a = new Set();
        a.append(0).append(1).append(2);
        Set b = new Set();
        b.append(0).append(1);
        assertFalse(a == b);        
        assertNotEquals(b, a);
        b.add(2);
        assertEquals(b, a);
        
        // !instance of Set
        assertFalse(a.equals(42));
        // size(a) != size(c)
    }
    
    @Test
    public void testHashCode() {
        Set a = new Set();
        a.add(42);
        Set b = new Set();
        b.add(42);
        Set c = new Set();
        c.append(42).add(13);
        
        assertEquals(a.hashCode(), b.hashCode());
        assertNotEquals(a.hashCode(), c.hashCode());
    }
    
    /**
     * Test parsing method
     */
    @Test
    public void testParse() {
        Set exp = new Set();
        exp.add(42);
        Set res = Set.parseSetOfInts("{42}");
        assertEquals(exp, res);
        
        exp = new Set().append(-42);
        assertEquals(exp, Set.parseSetOfInts("{-42}"));
        
        exp = new Set();
        exp.append(42).append(322).add(0);
        res = Set.parseSetOfInts("{42,322,0}");
        assertEquals(exp, res);
        
        // empty set
        assertEquals(new Set(), Set.parseSetOfInts("{}"));
        // non-set, constant
        assertNull(Set.parseSetOfInts("42"));
        // non-set, empty string
        assertNull(Set.parseSetOfInts(""));
        // parse null reference
        assertNull(Set.parseSetOfInts(null));
    }
    
    @Test
    public void testRetainAll() {
        Set s = new Set();
        s.retainAll(s);
    }
}
