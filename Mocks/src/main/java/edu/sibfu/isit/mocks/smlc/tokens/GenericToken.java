/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.smlc.tokens;

/**
 *
 * @author Maximillian M.
 * @param <T>
 */
public class GenericToken<T> extends Token {
    protected final T value;
    public GenericToken(T value) {
        this.value = value;
    }
    public T value() {
        return value;
    }
}
