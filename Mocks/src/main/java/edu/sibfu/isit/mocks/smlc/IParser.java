/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.smlc;

import edu.sibfu.isit.mocks.smlc.tokens.Token;

/**
 *
 * @author Maximillian M.
 */
public interface IParser {
    public Token parse(String input);
}
