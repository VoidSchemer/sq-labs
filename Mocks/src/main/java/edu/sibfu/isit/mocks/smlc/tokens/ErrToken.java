/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.smlc.tokens;

/**
 *
 * @author Maximillian M.
 */
public class ErrToken extends Token {

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ErrToken;
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }
}
