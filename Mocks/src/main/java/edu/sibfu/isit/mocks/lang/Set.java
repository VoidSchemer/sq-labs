/*
 * The MIT License
 *
 * Copyright 2015 Maximillian M..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package edu.sibfu.isit.mocks.lang;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Data type for math sets
 * @author Maximillian M.
 * @param <T>
 */
public class Set<T> implements Collection<T> {

    private final ArrayList<T> collection;

    public Set() {
        collection = new ArrayList<>();
    }
    /**
     * Creates a new set from collection. Takes only distinct elements
     * @param list the ArrayList whose elements are to be placed into this set
     */
    public Set(ArrayList<T> list) {
        collection = list.stream()
                .distinct()
                .collect(Collectors.toCollection(ArrayList<T>::new));
    }
    /**
     * Copies other set
     * @param set the set which is to be copied
     */
    public Set(Set<T> set) {
        collection = new ArrayList<>(set.collection);
    }

    @Override
    public int size() {
        return collection.size();
    }
    @Override
    public boolean isEmpty() {
        return collection.isEmpty();
    }
    @Override
    public boolean contains(Object o) {
        return collection.contains(o);
    }
    @Override
    public Iterator<T> iterator() {
        return collection.iterator();
    }
    @Override
    public Object[] toArray() {
        return collection.toArray();
    }
    @Override
    public <T> T[] toArray(T[] a) {
        return collection.toArray(a);
    }

    /**
     * Appends the specified element to the set.
     *
     * @param e element to be appended to this set
     * @return <tt>true</tt> if set does not contains such element, 
     *         <tt>false</tt> if set contains such element
     */
    @Override
    public boolean add(T e) {
        if (collection.contains(e)) {
            return false;
        } 
        
        collection.add(e);
        return true;
    }
    
    /**
     * Fluently appends the specified element to the set.
     * @param e element to be appended to this set
     * @return reference to this set
     */
    public Set<T> append(T e) {
        add(e);
        return this;
    }
    
    @Override
    public boolean remove(Object o) {
        return collection.remove(o);
    }
    @Override
    public boolean containsAll(Collection<?> c) {
        return collection.containsAll(c);
    }
    @Override
    public boolean addAll(Collection<? extends T> c) {
        return collection.addAll(c.stream()
                .distinct()
                .filter(entry -> collection.contains(entry) == false)
                .collect(Collectors.toCollection(ArrayList<T>::new))
        );
    }
    @Override
    public boolean removeAll(Collection<?> c) {
        return collection.removeAll(c);
    }
    @Override
    public boolean retainAll(Collection<?> c) {
        return collection.retainAll(c);
    }
    @Override
    public void clear() {
        collection.clear();
    }
   
    /**
     * Creates a new set as union of this and the specified set
     * @param set specified set 
     * @return union of this set and the specified set
     */
    public Set<T> union(Set<T> set) {
        Set newSet = new Set(this);
        newSet.addAll(set);
        return newSet;
    }
    /**
     * Creates a new set as intersection of this and the specified set
     * @param set specified set
     * @return intersection of this and the specified set
     */
    public Set<T> intersect(Set<T> set) {
        Set newSet = new Set();
        newSet.addAll(set.collection.stream()
                .filter(entry -> collection.contains(entry))
                .collect(Collectors.toCollection(ArrayList<T>::new))
        );
        return newSet;
    }
    /**
     * Creates a new set as subtraction of this and the specified set
     * @param set specified set
     * @return subtraction of this and the specified set
     */
    public Set<T> subtract(Set<T> set) {
        Set newSet = new Set(this);
        newSet.removeAll(set);
        return newSet;
    }
    /**
     * Checks if the given subset contains in this set. Duplicates containsAll
     * @param subset set whose elements are to be checked for presence in this set 
     * @return <tt>true</tt> if specified set is a subset, 
     *         <tt>false</tt> if specified set is not a subset
     */
    public boolean isSubset(Set<T> subset) {
        return containsAll(subset);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Set)) 
            return false;
       
        Set<?> otherSet = (Set<?>) obj;
        if (otherSet.size() != size())
            return false;
        
        return otherSet.containsAll(this);
    }

    @Override
    public int hashCode() {
        return collection.hashCode();
    }
    
    /**
     * Parses set of integers from string
     * @param s String which must be parsed. Example: {1,42,3}. Whitespace must be omitted
     * @return Parsed set of integers
     */
    public static Set parseSetOfInts(String s) {
        if (s == null) {
            return null;
        }
        
        Set<Integer> set = new Set<>();
        
        String number = "([+-]?\\d+)";
        String setPattern = "[{]((" + number + ",)*"+ number +")?[}]";
        Pattern pattern = Pattern.compile(setPattern, Pattern.CASE_INSENSITIVE);
        
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            pattern = Pattern.compile(number);
            matcher = pattern.matcher(s);
            while (matcher.find()) {
                String item = matcher.group();
                set.add(Integer.parseInt(item));
            }
        } else {
            return null;
        }
        
        return set;
    }
}