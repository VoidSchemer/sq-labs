/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.smlc.tokens;

/**
 *
 * @author Maximillian M.
 */
public class OpToken extends GenericToken<OpCode> {
    public OpToken(OpCode value) {
        super(value);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof OpToken)) {
            return false;
        }
        
        OpToken b = (OpToken) obj;
        return value.equals(b.value());
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
