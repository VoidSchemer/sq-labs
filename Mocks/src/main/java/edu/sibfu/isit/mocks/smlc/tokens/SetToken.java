/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.smlc.tokens;

import edu.sibfu.isit.mocks.lang.Set;

/**
 *
 * @author Maximillian M.
 */
public class SetToken extends GenericToken<Set> {
    public SetToken(Set value) {
        super(value);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SetToken)) {
            return false;
        }
        SetToken b = (SetToken) obj;
        return value.equals(b.value());
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
