/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.smlc;

import edu.sibfu.isit.mocks.lang.*;
import edu.sibfu.isit.mocks.smlc.tokens.*;

/**
 *
 * @author Maximillian M.
 */
public class SmlParser implements IParser {
    @Override
    public Token parse(String input) {
        input = input.replace(" ", "");
        switch (input){
            case "+":
                return new OpToken(OpCode.Union);
            case "-":
                return new OpToken(OpCode.Subtract);
            case "#":
                return new OpToken(OpCode.Intersect);
        }
        
        Set value = Set.parseSetOfInts(input);
        if (value != null) {
            return new SetToken(value);
        }
       
        return new ErrToken();
    }
}
