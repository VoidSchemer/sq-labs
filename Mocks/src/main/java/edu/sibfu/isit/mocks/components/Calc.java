/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sibfu.isit.mocks.components;

import edu.sibfu.isit.mocks.lang.Set;
import edu.sibfu.isit.mocks.smlc.IParser;
import edu.sibfu.isit.mocks.smlc.tokens.*;

/**
 *
 * @author Maximillian M.
 */
public class Calc {
   private enum State {
        WaitForInitValue,
        WaitForOperator,
        WaitForArgument
    }
    public enum Status {
        Success,
        Err_InvalidInput,
        Err_SetExpected,
        Err_BadOperation,
        Err_OperatorExpected,
        Failure
    }
 
    private final IParser parser;
    
    private Set acc;
    private OpCode lastOp;
    
    private State state;
    private Status status;
   
    public Calc(IParser parser) {
        this.parser = parser;
        acc = new Set();
        reset();
    }
    
    private void reset() {
        state = State.WaitForInitValue;
        lastOp = OpCode.Invalid;
    }
    
    public Status step(String input) {
        if (input == null) {
            return Status.Failure;
        }
        
        Token token = parser.parse(input);
        if (token instanceof ErrToken) {
            return Status.Err_InvalidInput;
        }
        
        switch (state) {
            case WaitForInitValue:
                return state_waitForInitValue(token);
            case WaitForOperator:
                return state_waitForOperator(token);
            case WaitForArgument:
                return state_waitForArgument(token);
            default:
                return Status.Failure;
        }
    }
    
    private Status state_waitForInitValue(Token t) {
        if (!(t instanceof SetToken)) {
            return Status.Err_SetExpected;
        }
        
        SetToken token = (SetToken) t;
        acc = token.value();
        state = State.WaitForOperator;
        return Status.Success;
    }
    
    private Status state_waitForOperator(Token t) {
        if (t instanceof EpsToken) {
            reset();
            return Status.Success;
        }
        if (!(t instanceof OpToken)) {
            return Status.Err_OperatorExpected;
        }
        
        OpToken token = (OpToken) t;
        lastOp = token.value();
        state = State.WaitForArgument;
        return Status.Success;
    }
    
    private Status state_waitForArgument(Token t) {
        if (!(t instanceof SetToken)) {
            return Status.Err_SetExpected;
        }
        SetToken token = (SetToken) t;
        Set newSet = exec(acc, token.value(), lastOp);
        if (newSet != null) {
            acc = newSet;
            state = State.WaitForOperator;
            return Status.Success;
        } else {
            return Status.Err_BadOperation;
        }
    } 
    
    private Set exec(Set a, Set b, OpCode op) {
        switch (op) {
            case Union:
                return a.union(b);
            case Subtract:
                return a.subtract(b);
            case Intersect:
                return a.intersect(b);
            default:
                return null;               
        }
    }
    
    public Set getAcc() {
        return acc;
    }
}
